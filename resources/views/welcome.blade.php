<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <script src="{{ mix('/js/app.js') }}"></script>
</head>

<body class="grey lighten-5">
    <div class="container _main-holder">
        <div class="row _m-btm-0">
            <div class="col s10 offset-s1">
                <div class="right-align">
                    <a class="waves-effect waves-light btn-small white black-text" href="{{ route('logout') }}">Log Out
                    </a>
                </div>
                <h1 class="_title-main blue-grey-text darken-4"><a href="#" class="_title-fs-holder"><span
                            class="_title-fs">Flyer School</span></a></span><span
                        class="_title-tk">Data</span></h1>
            </div>
        </div>
        <div class="_main-content">
            <div id="timekeeping-container">
                <div class="row">
                    <div class="col s10 offset-s1">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('transform') }}">
                            @csrf
                            <div class="card-panel z-depth-1 _entries-holder animated flipInX fast _inactive">
                                <h2 class="_m-btm-0">Transform CSV file</h2>
                                <div style="margin-bottom: 15px;">
                                    <span style="font-weight: 300;">Based on the following shared examples: 
                                    <br> - <a href="files/FLYER UPLOAD_ Advisor Groups - Flyer Info V2 (1).csv">FLYER UPLOAD_ Advisor Groups - Flyer Info V2</a> <i>Students first</i>
                                    <br> - <a href="files/Flyer_UserImport.csv">Flyer_UserImport</a> <i>Contacts first</i>
                                    </span>
                                </div>
                                <div class="_entries-form-container">
                                    <div>
                                    <span class="grey-text">Input file type</span>
                                    <p>
                                        <label>
                                            <input name="transform_type" type="radio" value="1" checked />
                                            <span class="black-text">Students first</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                            <input name="transform_type" type="radio" value="2" />
                                            <span class="black-text">Contacts first</span>
                                        </label>
                                    </p>
                                    </div>
                                    <div class="row">
                                        <div class="col s6">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>CSV FILE</span>
                                                    <input type="file" name="dataset_file" />
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="right-align">
                                <button type="submit" class="waves-effect waves-light btn-large indigo lighten-1">Transform & Download</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col s10 offset-s1">
                        <form method="POST" action="{{ route('generate') }}">
                            @csrf
                            <div class="card-panel z-depth-1 _entries-holder animated flipInX fast _inactive">
                                <h2>Get School data</h2>
                                <div class="row">
                                    <div class="col s12 m8">
                                        <div class="card-panel _panel-sm">
                                            <span>Firstly select a <b>district</b> and then a <b>school</b> you want to export user data for.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="_entries-form-container">
                                    <div class="row">
                                        <div class="col s6">
                                            <div style="margin-bottom: 15px;">
                                                <span class="grey-text">District</span>
                                            </div>
                                            <div class="input-field">
                                                <i class="material-icons prefix">map</i>
                                                <select class="_tk-select2" id="select-districts" name="district">
                                                    <option></option>
                                                    @foreach($districts as $district)
                                                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s6">
                                            <div style="margin-bottom: 15px;">
                                                <span class="grey-text">School</span>
                                            </div>
                                            <div class="input-field">
                                                <i class="material-icons prefix">school</i>
                                                <select class="_tk-select2" id="select-schools" name="school">
                                                    <option></option>
                                                </select>
                                            </div>
                                            <div class="progress _invisible" id="progress-schools">
                                                <div class="indeterminate"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="width: 350px; float: right; padding-right: 15px;" id="stats-holder" class="hide">
                                        <div class="card-panel _panel-sm teal">
                                            <span class="white-text"><big>Number of users</big></span>
                                            <div>
                                                <span class="white-text">School level: <big><b id="school-users-count"></b></big></span>
                                                <span class="white-text" style="margin-left: 10px;">Group level: <big><b id="group-users-count"></b></big></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="right-align">
                                <button type="submit" class="waves-effect waves-light btn-large indigo lighten-1" id="tk-dl-btn" disabled="disabled">Download
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<script>
    var APP_URL = {!! json_encode(url('/')) !!};
    console.log(APP_URL);
    $(document).ready(function () {
        $("#select-districts").on("change", function(e) {
            console.log($(this).val());
            var selectedDistrict = $(this).val();
            if (selectedDistrict != "" && selectedDistrict != null) {
                $("#progress-schools").removeClass("_invisible").addClass("_visible");
                $("#select-schools").attr("disabled", "disabled").parent().toggleClass("_disabled");
                $("#tk-dl-btn").attr("disabled", "disabled");
                $.ajax({
                    method: "GET",
                    url: APP_URL + "/ajax/units/" + selectedDistrict + "/schools",
                    dataType: "json"
                })
                    .done(function (r) {
                        console.log(r);
                        var schools = [];
                        $.each(r, function(index, value) {
                            var countsTxt = "(" + value.general_users + ")";
                            schools.push({
                                id: value.id,
                                text: value.name
                            });
                        });
                        $("#select-schools").empty()
                            .select2({ placeholder: "Select an item", data: schools, allowClear: true })
                            .val("")
                            .trigger('change');

                        $("#progress-schools").removeClass("_visible").addClass("_invisible");
                        $("#select-schools").removeAttr("disabled").parent().toggleClass("_disabled");
                    })
                    .fail(function (jqXHR, status) {
                        console.log(jqXHR);
                    });
            }
            else {
                $("#select-schools").empty()
                            .select2({ placeholder: "Select an item", data: [], allowClear: true })
                            .val("")
                            .trigger('change');
                $("#tk-dl-btn").attr("disabled", "disabled");
            }
        });

        $("#select-schools").on("change", function(e) {
            console.log($(this).val());
            if ($(this).val() != "" && $(this).val() != null) {
                $("#tk-dl-btn").removeAttr("disabled");
                $("#stats-holder").removeClass("hide");

                $.ajax({
                    method: "GET",
                    url: APP_URL + "/ajax/units/schools/" + $(this).val() + "/stats",
                    dataType: "json"
                })
                    .done(function (r) {
                        console.log(r);
                        $("#school-users-count").html(r.school_users);
                        $("#group-users-count").html(r.group_users);
                    })
                    .fail(function (jqXHR, status) {
                        console.log(jqXHR);
                    });
            }
            else {
                $("#tk-dl-btn").attr("disabled", "disabled");
                $("#stats-holder").addClass("hide");
            }
        });

        M.updateTextFields();
        //swal("Hello world!");

        $("#tk-input-description").characterCounter();

        $("._tk-select2").select2({
            placeholder: "Select an item",
            allowClear: true
        });
        $('.sidenav').sidenav();

        $('.datepicker').datepicker({
            maxDate: new Date(),
            format: "mmmm dd, yyyy"
        });

        $('._main-content').resize(function () {
            console.log("resize");
        });
    });

    $("#tk-new-entry-btn").click(function () { addNewEntryFormElement(); })

    $("#tk-input-date").on("change", function (e) {
        if (_.isEmpty($(this).attr("data-date"))) {
            addNewEntryFormElement();
        }
        $(this).attr("data-date", $(this).val());
    });
</script>