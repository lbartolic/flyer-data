<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    <form method="POST" action="{{ route('post-auth') }}">
        @csrf
        Identifier:
        <input type="text" name="identifier"/>
        Secret:
        <input type="password" name="secret"/>
        <button type="submit">Submit</button>
    </form>
</body>
</html>