<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Unit\Unit;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function units() {
        return $this->belongsToMany(Unit::class, "users_units");
    }

    public function relatedContacts() {
        return $this->belongsToMany(User::class, "user_relationships", "student_user_id", "contact_user_id");
    }

    public function relatedStudents() {
        return $this->belongsToMany(User::class, "user_relationships", "contact_user_id", "student_user_id");
    }
}
