<?php

namespace App\Models\Unit;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Unit extends Model
{
    protected $table = "units";

    public function users() {
        return $this->belongsToMany(User::class, "users_units");
    }
}
