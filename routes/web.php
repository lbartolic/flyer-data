<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => "has-auth"], function() {
    Route::get('/', function () {
        $districts = App\Models\Unit\Unit::where("unit_type_id", 1)->orderBy("name")->get();
        return view('welcome')->with([
            "districts" => $districts
        ]);
    })->name("welcome");

    Route::group(["prefix" => "ajax"], function() {
        Route::get("units/{unit_id}/schools", function($unitId) {
            $schools = App\Models\Unit\Unit::where("parent_id", $unitId)->withCount([
                "users as general_users" => function($q) {
                    return $q->where("role_id", 3);
                }
            ])->orderBy("name")->get();

            return $schools;
        });
        Route::get("units/schools/{unit_id}/stats", function($unitId) {
            $schoolId = $unitId;
            $groupIds = App\Models\Unit\Unit::where("parent_id", $schoolId)->get(["id"])->pluck("id");
            $groupUsers = App\User::whereIn("role_id", [3, 4])
                ->whereHas("units", function($q) use($groupIds) {
                    $q->whereIn("unit_id", $groupIds);
                })
                ->get(["id"]);
            $schoolUsers = App\User::whereIn("role_id", [3, 4])
                ->whereHas("units", function($q) use($schoolId) {
                    $q->where("unit_id", $schoolId);
                })
                ->get(["id"]);
            
            return [
                "group_users" => $groupUsers->count(), 
                "school_users" => $schoolUsers->count()
            ];
        });
    });

    Route::post("/transform", function(Illuminate\Http\Request $request) {
        $file = $request->file("dataset_file");
        $type = $request->input("transform_type");
        $importResults = Excel::load($file, function($reader) {})->get();
        //dd($importResults);
        $dataArray = $importResults->toArray();
        if ($type == 1) {
            $contactFields = [
                "contact_first_name", 
                "contact_last_name", 
                "contact_email", 
                "contacts.relationship",
                "phone",
                "phone2",
                "phone_3",
                "address",
                "add_ln_2"
            ];
            $studentFields = [
                "first_name_student", 
                "last_name_student", 
                "grade",
                "address",
                "add_ln_2"
            ];
        }
        else if ($type == 2) {
            $contactFields = [
                "contacts_firstname", 
                "contacts_lastname", 
                "contacts_cellphone", 
                "contacts_email"
            ];
            $studentFields = [
                "student_firstname", 
                "student_lastname", 
                "student_grade", 
                "student_studentnumber"
            ];
        }
        $contacts = [];
        $students = [];
        foreach($dataArray as $entryKey => $value) {
            foreach($contactFields as $contactField) $contacts[$entryKey][$contactField] = $value[$contactField];
            foreach($studentFields as $studentField) $students[$entryKey][$studentField] = $value[$studentField];
        }

        $filteredContacts = [];
        $filteredStudents = [];
        $index = 0;
        foreach($contacts as $entryKey => $entry) {
            $relatedStudents = [];
            $matchesContacts = array_where($filteredContacts, function($value, $key) use(
                $contactFields, $studentFields, $type, $entryKey, &$filteredContacts, &$relatedStudents, $entry, $students
            ) {
                if ($type == 1) {
                    $fnKey = $contactFields[0];
                    $lnKey = $contactFields[1];
                    $pKey = $contactFields[4];
                    $p2Key = $contactFields[5];
                }
                else if ($type == 2) {
                    $fnKey = $contactFields[0];
                    $lnKey = $contactFields[1];
                    $pKey = $contactFields[2];
                    $p2Key = $contactFields[2];
                }
                // check if phone numbers/first name/last name matches to determine if the contact should be treated as 1 record (the same person)
                if ($value[$fnKey] == $entry[$fnKey] && $value[$lnKey] == $entry[$lnKey] && $value[$pKey] == $entry[$pKey] && $value[$p2Key] == $entry[$p2Key]) {
                    if (!in_array($entryKey, $filteredContacts[$key]["related_students"])) {
                        array_push($filteredContacts[$key]["related_students"], [
                            "num" => $entryKey+1,
                            $studentFields[0] => $students[$entryKey][$studentFields[0]],
                            $studentFields[1] => $students[$entryKey][$studentFields[1]]
                        ]);
                    }
                    return true;
                }
                elseif ($key == $entryKey) {
                }
                return false;
            });
            if (count($matchesContacts) == 0) {
                $filteredContacts[$index] = $entry;
                $filteredContacts[$index]["related_students"] =  [[
                    "num" => $entryKey+1,
                    $studentFields[0] => $students[$entryKey][$studentFields[0]],
                    $studentFields[1] => $students[$entryKey][$studentFields[1]]
                ]];
                $index++; 
            }
        }

        //dd($filteredContacts);

        foreach($students as $entryKey => $entry) {
            $matchesStudents = array_where($filteredStudents, function($value, $key) use(
                $contactFields, $studentFields, $type, $entryKey, &$filteredContacts, &$filteredStudents, $entry
            ) {
                if ($type == 1) {
                    $fnKey = $studentFields[0];
                    $lnKey = $studentFields[1];
                    $aKey = $studentFields[3];
                }
                else if ($type == 2) {
                    $studentNumber = $studentFields[3];
                    $fnKey = $lnKey = $aKey = $studentNumber;
                }
                // check if address/first name/last name matches to determine if the student should be treated as 1 record (the same person)
                // in case $type is 2, STUDENT NUMBER can be used to determine this, so all criteria is set to STUDENT NUMBER value (TODO: refactor code...)
                if ($value[$fnKey] == $entry[$fnKey] && $value[$lnKey] == $entry[$lnKey] && $value[$aKey] == $entry[$aKey]) {
                    return true;
                }
                elseif ($key == $entryKey) {
                }
                return false;
            });
            if (count($matchesStudents) == 0) $filteredStudents[] = $entry;
        }

        //dd(array_slice($filteredContacts, 0, 10));

        $cFnKey = $cLnKey = $cEKey = $cPKey = $cP2Key = $cP3Key = null;
        $sFnKey = $sLnKey = $sGKey = $sSnKey = null;
        if ($type == 1) {
            $cFnKey = $contactFields[0];
            $cLnKey = $contactFields[1];
            $cEKey = $contactFields[2];
            $cPKey = $contactFields[4];
            $cP2Key = $contactFields[5];
            $cP3Key = $contactFields[6];
            $sFnKey = $studentFields[0];
            $sLnKey = $studentFields[1];
            $sGKey = $studentFields[2];
        }
        else if ($type == 2) {
            $cFnKey = $contactFields[0];
            $cLnKey = $contactFields[1];
            $cEKey = $contactFields[3];
            $cPKey = $contactFields[2];
            $sFnKey = $studentFields[0];
            $sLnKey = $studentFields[1];
            $sGKey = $studentFields[2];
            $sSnKey = $studentFields[3];
        }
        $exportFields = ["first_name", "last_name", "username", "email", "country_code", "mobile_phone_number", "dob", "language", "organization_id", "user_type", "relationship_definition", "clever_id", "grade", "sis_id", "student_number", "student_id"];
        $exportContactsData = [];
        $exportStudentsData = [];
        foreach($filteredContacts as $key => $contact) {
            foreach($exportFields as $exportField) $exportContactsData[$key][$exportField] = null;
            $exportContactsData[$key]["first_name"] = $contact[$cFnKey];
            $exportContactsData[$key]["last_name"] = $contact[$cLnKey];
            $exportContactsData[$key]["email"] = $contact[$cEKey];
            if ($contact[$cPKey] != null) $exportContactsData[$key]["mobile_phone_number"] = $contact[$cPKey];
            elseif (isset($cP2Key) && $contact[$cP2Key] != null) $exportContactsData[$key]["mobile_phone_number"] = $contact[$cP2Key];
            elseif (isset($cP3Key) && $contact[$cP3Key] != null) $exportContactsData[$key]["mobile_phone_number"] = $contact[$cP3Key];
            $exportContactsData[$key]["user_type"] = 3;
            $relatedUsersArr = [];
            foreach($contact["related_students"] as $relatedStudent) {
                $relatedUsersStr = "[" . $relatedStudent["num"] . "] " . $relatedStudent[$sLnKey] . " " . $relatedStudent[$sFnKey];
                array_push($relatedUsersArr, $relatedUsersStr);
            }
            $exportContactsData[$key]["related_students"] = implode($relatedUsersArr, " , ");
            
        }
        //dd($exportContactsData);
        foreach($filteredStudents as $key => $student) {
            foreach($exportFields as $exportField) $exportStudentsData[$key][$exportField] = null;
            $exportStudentsData[$key]["first_name"] = $student[$sFnKey];
            $exportStudentsData[$key]["last_name"] = $student[$sLnKey];
            $exportStudentsData[$key]["grade"] = (int)$student[$sGKey];
            if (isset($sSnKey)) $exportStudentsData[$key]["student_number"] = (int)$student[$sSnKey];
            $exportStudentsData[$key]["user_type"] = 4;
        }

        //dd($exportStudentsData, $exportContactsData);
        
        $fileName = "transformed_" . Carbon\Carbon::now()->format("Y-m-d-H:me");
        Maatwebsite\Excel\Facades\Excel::create($fileName, function($excel) use($exportContactsData, $exportStudentsData, $exportFields) {
            $excel->sheet("CONTACTS", function($sheet) use($exportContactsData, $exportFields) {
                $sheet->fromArray($exportContactsData, null, 'A1', true, false);
                $sheet->prependRow($exportFields);
            });
            $excel->sheet("STUDENTS", function($sheet) use($exportStudentsData, $exportFields) {
                $sheet->fromArray($exportStudentsData, null, 'A1', true, false);
                $sheet->prependRow($exportFields);
            });
        })->export('xls');

    })->name("transform");

    Route::post("/generate", function(Illuminate\Http\Request $request) {
        $userFields = ["id", "username", "email", "first_name", "last_name", "role_id", "country_code", "phone_number", "token", "city"];
        $schoolId = $request->input("school");
        $groupIds = App\Models\Unit\Unit::where("parent_id", $schoolId)->get(["id"])->pluck("id");
        $groupUsers = App\User::with([
            "units" => function($q) {
                return $q->where("unit_type_id", 3);
            },
            "relatedContacts"])
            ->whereIn("role_id", [3, 4])
            ->whereHas("units", function($q) use($groupIds) {
                $q->whereIn("unit_id", $groupIds);
            })
            ->get($userFields);
        $schoolUsers = App\User::with([
            "units" => function($q) {
                return $q->where("unit_type_id", 2);
            },
            "relatedContacts"])
            ->whereIn("role_id", [3, 4])
            ->whereHas("units", function($q) use($schoolId) {
                $q->where("unit_id", $schoolId);
            })
            ->get($userFields);

        $relatedContactIds = [];    
        $groupUsers->each(function($item, $key) use(&$relatedContactIds) {
            $unitIds = $item->units->pluck("id")->toArray();
            $unitNames = $item->units->pluck("name")->toArray();
            $item["unit_ids_formatted"] = implode($unitIds, " | ");
            $item["unit_names_formatted"] = implode($unitNames, " | ");
            $studentRelatedContactIds = $item->relatedContacts->pluck("id");
            foreach($studentRelatedContactIds as $contactId) {
                if (!in_array($contactId, $relatedContactIds)) {
                    $relatedContactIds[] = $contactId;
                }
            }
            unset($item["units"], $item["relatedContacts"]);
            return $item;
        });
        $schoolUsers->each(function($item, $key) use(&$relatedContactIds) {
            $unitIds = $item->units->pluck("id")->toArray();
            $unitNames = $item->units->pluck("name")->toArray();
            $item["unit_ids_formatted"] = implode($unitIds, " | ");
            $item["unit_names_formatted"] = implode($unitNames, " | ");
            $studentRelatedContactIds = $item->relatedContacts->pluck("id");
            foreach($studentRelatedContactIds as $contactId) {
                if (!in_array($contactId, $relatedContactIds)) {
                    $relatedContactIds[] = $contactId;
                }
            }
            unset($item["units"], $item["relatedContacts"]);
            return $item;
        });
        $relatedContacts = App\User::with([
            "relatedStudents", 
            "relatedStudents.units" => function($q) {
                $q->where("unit_type_id", 3);
            }])
            ->whereIn("id", $relatedContactIds)
            ->get($userFields);
        $relatedContacts->each(function($item, $key) {
            $relatedStudentsFormatted = "";
            $item->relatedStudents->map(function ($item, $key) use(&$relatedStudentsFormatted) {
                $relatedStudentsFormatted .= $item->id . ": [" . $item->first_name . " " . $item->last_name . "] ";
            });
            $item["related_students_formatted"] = $relatedStudentsFormatted;
            
            return $item;
        });
        $relatedContacts->each(function($item, $key) {
            $relatedGroupIds = [];
            $relatedGroupNames = [];
            $relatedGroupInfo = [];
            $item->relatedStudents->each(function($relStudItem, $relStudKey) use(&$relatedGroupIds, &$relatedGroupNames) {
                array_push($relatedGroupIds, $relStudItem->units->pluck("id"));
                array_push($relatedGroupNames, $relStudItem->units->pluck("name"));
            });
            $item["related_group_ids_formatted"] = implode(array_flatten($relatedGroupIds), " | ");
            $item["related_group_names_formatted"] = implode(array_flatten($relatedGroupNames), " | ");
        });

        $logString = "Date/Time: " . Carbon\Carbon::now()->format("Y-m-d H:m") . " ; School: " . $schoolId . " ; Groups: " . implode($groupIds->toArray(), ",");
        $logString .= "\r\n";
        $logString .= "IP address: " . $request->ip();
        $logString .= "\r\n";
        $logString .= "User agent: " . $request->header("User-Agent");
        $logString .= "\r\n";
        $logString .= "----------------------------------------";
        Storage::append('file.log', $logString);
        
        $exportData = [$groupUsers->toArray(), $schoolUsers->toArray()];
        $exportFields = $userFields;
        $relContactsExportFields = $userFields;
        array_push($exportFields, "unit_ids", "unit_names");
        array_push($relContactsExportFields, "related_students", "related_students_group_ids", "related_students_group_names");
        
        $fileName = "export_" . Carbon\Carbon::now()->format("Y-m-d-H:me");
        Maatwebsite\Excel\Facades\Excel::create($fileName, function($excel) use($groupUsers, $schoolUsers, $relatedContacts, $exportFields, $relContactsExportFields) {
            $excel->sheet("GROUP LEVEL", function($sheet) use($groupUsers, $exportFields) {
                $sheet->fromArray($groupUsers->toArray(), null, 'A1', true, false);
                $sheet->prependRow($exportFields);
            });
            $excel->sheet("SCHOOL LEVEL", function($sheet) use($schoolUsers, $exportFields) {
                $sheet->fromArray($schoolUsers->toArray(), null, 'A1', true, false);
                $sheet->prependRow($exportFields);
            });
            $excel->sheet("RELATED CONTACTS", function($sheet) use($relatedContacts, $relContactsExportFields) {
                $sheet->fromArray($relatedContacts->toArray(), null, 'A1', true, false);
                $sheet->prependRow($relContactsExportFields);
            });
        })->export('xls');
    })->name('generate');
});

Route::get("auth", function() {
    return view('auth');
})->name('auth');

Route::post("auth", function(Illuminate\Http\Request $request) {
    $creds = ["FlyerSchoolClient2018", "49tegrij.ret4w!"];
    $identifier = $request->input("identifier");
    $secret = $request->input("secret");
    if ($identifier == $creds[0] && $secret == $creds[1]) {
        $request->session()->push('auth', true);
        return redirect()->route('welcome');
    }
    else {
        return redirect()->route('auth');
    }
})->name('post-auth');

Route::get("logout", function(Illuminate\Http\Request $request) {
    $request->session()->forget('auth');
    return redirect()->back();
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/test", function(Illuminate\Http\Request $request) {

    dd(App\User::with(["relatedContacts"])->where("id", 5888)->get()->pluck("relatedContacts")->toArray());
    dd($request);
    $schools = App\Models\Unit\Unit::where("parent_id", 188)->withCount([
        "users as general_users" => function($q) {
            return $q->where("role_id", 3);
        }
    ])->get(['id']);

    $schoolIds = array_pluck($schools->toArray(), "id");

    $groups = App\Models\Unit\Unit::whereIn("parent_id", $schoolIds)->get(["id"])->pluck("id");
    //dd($schoolIds);

    Maatwebsite\Excel\Facades\Excel::create("test", function($excel) {
        $excel->sheet("testsheet", function($sheet) {
            $sheet->fromArray([["asdas", "asdasd"], ["asdas", "asdasd"]], null, 'A1', true, false);
            $sheet->prependRow(["1", "2"]);
        });
    })->export('xls');
    dd(array_merge($groups->toArray(), $schoolIds));

    return dd(App\User::whereNotNull("token")->whereHas("units", function($q) { return $q->where("units.id", 16); })->limit(100)->get());
});