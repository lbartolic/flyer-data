const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy(
    "node_modules/font-awesome/fonts",
    "public/fonts"
)
    .sass("./resources/sass/app.scss", "../resources/css/build/compiled.css")

    .styles([
        "./resources/css/plugins/animate.css",
        "./node_modules/nouislider/distribute/nouislider.min.css",
        "./node_modules/select2/dist/css/select2.min.css",
        "./node_modules/materialize-css/dist/css/materialize.min.css",
        "./node_modules/materialize-css/extras/noUiSlider/nouislider.css",
        "./node_modules/font-awesome/css/font-awesome.min.css",
        "./resources/css/build/compiled.css"
    ], "./public/css/app.css")

    .scripts([
        "./node_modules/jquery/dist/jquery.min.js",
        "./node_modules/select2/dist/js/select2.min.js",
        "./node_modules/sweetalert/dist/sweetalert.min.js",
        "./node_modules/nouislider/distribute/nouislider.min.js",
        "./node_modules/materialize-css/dist/js/materialize.min.js",
        "./node_modules/materialize-css/extras/noUiSlider/nouislider.min.js",
        "./node_modules/underscore/underscore-min.js",
        "./node_modules/moment/min/moment-with-locales.min.js",
        "./node_modules/handlebars/dist/handlebars.min.js",
        "./resources/js/app.js"
    ], "./public/js/app.js");